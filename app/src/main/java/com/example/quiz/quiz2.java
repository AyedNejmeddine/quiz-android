package com.example.quiz;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class quiz2 extends AppCompatActivity {
    Button suivantButton, retourneutton;
    RadioGroup radioGroup;
    RadioButton radio2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz2);

        Intent intent = getIntent();
        int score = Integer.valueOf(intent.getStringExtra("score"));

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radio2 = (RadioButton) findViewById(R.id.radio2);

        suivantButton =  (Button) findViewById(R.id.buttonSuivant);
        suivantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1){
                    AlertDialog.Builder alert = new AlertDialog.Builder(quiz2.this);
                    alert.setTitle("Attention").setMessage("Il faut cocher l'un des reponse !")
                            .setPositiveButton("Ok", null).show();
                } else {
                    int newScore = score;
                    if (radio2.isChecked()){
                        newScore = score + 1;
                    }
                    Intent intent = new Intent(quiz2.this, quiz3.class);
                    intent.putExtra("score", String.valueOf(newScore));
                    startActivity(intent);
                }

            }
        });

        retourneutton =  (Button) findViewById(R.id.buttonRetourne);
        retourneutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}