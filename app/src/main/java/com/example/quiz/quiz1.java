package com.example.quiz;

import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class quiz1 extends AppCompatActivity {
    Button suivantButton, retourneutton;
    RadioGroup radioGroup;
    RadioButton radio3;
    int score = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz1);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radio3 = (RadioButton) findViewById(R.id.radio3);

        suivantButton =  (Button) findViewById(R.id.buttonSuivant);
        suivantButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radioGroup.getCheckedRadioButtonId() == -1){
                    AlertDialog.Builder alert = new AlertDialog.Builder(quiz1.this);
                    alert.setTitle("Attention").setMessage("Il faut cocher l'un des reponse !")
                            .setPositiveButton("Ok", null).show();
                } else {
                    if (radio3.isChecked()){
                        score = 1;
                    }
                    Intent intent = new Intent(quiz1.this, quiz2.class);
                    intent.putExtra("score", String.valueOf(score));
                    startActivity(intent);
                }
            }
        });

        retourneutton =  (Button) findViewById(R.id.buttonRetourne);
        retourneutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}